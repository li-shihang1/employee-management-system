

#include "../inc/handler.h"
#include  "../inc/sql.h"

int initMsg(msg_t *msg)
{
    msg->cmd = 0,
    msg->condition = 0;

    msg->staff.age = 0;
    strcpy(msg->staff.department, " ");
    msg->staff.id = 0;
    strcpy(msg->staff.name, " ");
    msg->staff.salary = 0;
    msg->staff.isadmin = 0;
    strcpy(msg->staff.sex, " ");
    strcpy(msg->staff.password, " ");

    return 0;
}

int userRegister(msg_t *msg, int fd, sqlite3 *db)
{
    if (msg->cmd == (REGISTER | ISADMIN))
        msg->staff.isadmin = 1;

    queryByNameOrId(msg, db);
    if (msg->staff.id != 0)
    {
        printf("userRegister: id is not 0\n");
        msg->condition = 0;
        return -1;
    }
    printf("msg.staff.id = %d\n", msg->staff.id);
    if (insertUser(msg, db))
    {
        msg->condition = 0;
        return -1;
    }
    queryByNameOrId(msg, db);

    msg->condition = 1;

    return 0;
}

int userLogin(msg_t *msg, int fd, sqlite3 *db)
{
    char password[42];
    strcpy(password, msg->staff.password);
    queryByNameOrId(msg, db);
    if (msg->staff.id == 0)
    {
        printf("userLogin: user not exist\n");
        msg->condition = 0;
        return -1;
    }

    if (strcmp(msg->staff.password, password))
    {
        printf("userLogin: password is not right\n");
        msg->condition = 0;
        return -1;
    }
    msg->condition = 1;

    return 0;
}
int changePassword(msg_t *msg, int fd, sqlite3 *db)
{
    updataUserInfo(msg, db);
    return 0;
}
int changeInfo(msg_t *msg, int fd, sqlite3 *db)
{
    updataUserInfo(msg, db);
    // msg->condition = 1;
    return 0;
}
int addUser(msg_t *msg, int fd, sqlite3 *db)
{
    char name[42];
    strcpy(name, msg->staff.name);
    queryByNameOrId(msg, db);
    if (msg->staff.id)
    {
        printf("userLogin: user exist\n");
        msg->condition = 0;
        return -1;
    }
    insertUser(msg, db);
    queryByNameOrId(msg, db);
    msg->condition = 1;
    return 0;
}
int deletUser(msg_t *msg, int fd, sqlite3 *db)
{
    
    queryByNameOrId(msg, db);
   
    if (msg->staff.id == 0)
    {
        printf("userLogin: user not exist\n");
        msg->condition = 0;
        return -1;
    }
    if (deleteUser(msg, db))
    {
        msg->condition = 0;
        return -1;
    }
    msg->condition = 1;
    return 0;
}
int modifyInfo(msg_t *msg, int fd, sqlite3 *db)
{
    if (msg->cmd == (CHANGE_USER_INFO | SELECT_ONE))
    {
        msg->staff.id = msg->condition;
        queryByNameOrId(msg, db);
        return 0;
    }

    if(updateByCondition(msg, db))
    {
        return -1;
    }
    msg->condition = 1;
    return 0;
}
int selectUserInfo(msg_t *msg, int fd, sqlite3 *db){
    if (msg->cmd == (SELECT_USER_INFO|TOP_RECORD))
    {
        queryByTopRecord(msg, db);
        return 0;
    }else if (msg->cmd == (SELECT_USER_INFO|BOTTOM_RECORD))
    {
        queryByBottomRecord(msg, db);
        return 0;
    }else if (msg->cmd == (SELECT_USER_INFO|BY_CONDITION))
    {
        queryByCondition(msg, db);
        return 0;
    }
    queryAll(msg, db);
    
    return 0;

}
int searchHistory(msg_t *msg, int fd, sqlite3 *db){
if (msg->cmd == (SELECT_ALL|BY_CONDITION))
    {
        queryHistoryCondition(msg, db);
        return 0;
    }else if (msg->cmd == (SELECT_ALL|BOTTOM_RECORD))
    {
        queryHistoryBottom(msg, db);
        return 0;
    }
    
    queryHistoryAll(msg, db);
    
    return 0;
}