
#include "../inc/head.h"

/**
 * @description: 初始化 msg 结构体
 * @param {msg_t} *msg
 * @return {*}
 */
int initMsg(msg_t *msg)
{
    msg->cmd = 0,
    msg->condition = 0;

    msg->staff.age = 0;
    strcpy(msg->staff.department, " ");
    msg->staff.id = 0;
    strcpy(msg->staff.name, " ");
    msg->staff.salary = 0;
    msg->staff.isadmin = 0;
    strcpy(msg->staff.sex, " ");
    strcpy(msg->staff.password, " ");

    return 0;
}

int myFgets(char *str, int size, FILE *file)
{
    fgets(str, size, stdin);
    str[strlen(str) - 1] = '\0';
    return 0;
}
/**
 * @Author: yuya 2844956842@qq.com
 * @Date: 2023-08-19 20:24:35
 * @description: 连接服务器
 * @param {char} *ip
 * @param {int} port
 * @return {int 套接字}
 */
int connectSocket(const char *ip, int port)
{
    // 1.创建TCP的流式套接字：
    int sockFd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockFd == -1)
    {
        perror("socket error\n");
        return -1;
    }
    // 2.构建AF_INET通信域地址信息结构体：
    struct sockaddr_in serverAddInfo = {0};
    serverAddInfo.sin_family = AF_INET;
    serverAddInfo.sin_port = htons(port);
    serverAddInfo.sin_addr.s_addr = inet_addr(ip);
    // serverAddInfo.sin_addr.s_addr = ip;
    if ((connect(sockFd, (const struct sockaddr *)&serverAddInfo, sizeof(serverAddInfo))) == -1)
    {
        perror("connect error");
        return -1;
    }
    return sockFd;
}

/**
 * @Author: yuya 2844956842@qq.com
 * @Date: 2023-08-19 19:54:29
 * @description: 管理员修改员工信息的选项
 * @return {void}
 */
void ministerModifyChooseShow()
{
    printf("********************************************\n");
    printf("***************** 1.修改姓名 ***************\n");
    printf("***************** 2.修改年龄 ***************\n");
    printf("***************** 3.修改性别 ***************\n");
    printf("***************** 4.修改密码 ***************\n");
    printf("***************** 5.修改工号 ***************\n");
    printf("***************** 6.修改部门 ***************\n");
    printf("***************** 7.修改工资 ***************\n");
    printf("******************* 8.退出 *****************\n");
    printf("********************************************\n");
}

/**
 * @description: 管理员查询选项
 * @return {*}
 */
void selectChooseShow()
{
    printf("*************************************************\n");
    printf("*************** 1.所有用户信息 ******************\n");
    printf("*************** 2.前10个用户 ********************\n");
    printf("*************** 3.后10个用户 ********************\n");
    printf("*************** 4.条件查询 **********************\n");
    printf("*************************************************\n");
}

/**
 * @Author: yuya 2844956842@qq.com
 * @Date: 2023-08-17 19:06:29
 * @description: 打印表头
 * @return 无
 */
void printShow()
{
    printf("****************************************\n");
    printf("***************** 1.注册 ***************\n");
    printf("***************** 2.登录 ***************\n");
    printf("***************** 3.退出 ***************\n");
    printf("****************************************\n");
}

/**
 * @Author: yuya 2844956842@qq.com
 * @Date: 2023-08-19 17:20:00
 * @description: 打印普通员工需要修改的选择
 * @return {void}
 */
void printModifyInfo()
{
    printf("******************************************\n");
    printf("*************** 1.修改姓名 ***************\n");
    printf("*************** 2.修改年龄 ***************\n");
    printf("*************** 3.修改性别 ***************\n");
    printf("******************************************\n");
}

/**
 * @Author: yuya 2844956842@qq.com
 * @Date: 2023-08-17 19:28:35
 * @description: 打印普通用户选择功能菜单
 * @return 无
 */
void normalShow()
{
    printf("*****************************************\n");
    printf("*************** 1.查询信息 ***************\n");
    printf("*************** 2.修改密码 ***************\n");
    printf("************** 3.修改个人信息 *************\n");
    printf("************** 4.查询历史记录 ************\n");
    printf("***************** 5. 退出****************\n");
    printf("*****************************************\n");
}

/**
 * @Author: yuya 2844956842@qq.com
 * @Date: 2023-08-17 19:57:05
 * @description: 打印管理员用户选择功能菜单
 * @return 无
 */
void managerShow()
{
    printf("*****************************************\n");
    printf("*************** 1.添加用户 ***************\n");
    printf("*************** 2.删除用户 ***************\n");
    printf("************* 3.修改用户信息 *************\n");
    printf("************* 4.查询用户信息 *************\n");
    printf("************** 5. 查询历史 ***************\n");
    printf("**************** 6.退出 *****************\n");
}

/**
 * @description: 输入用户名密码
 * @param {msg_t} *msg msg结构体
 * @param {int} sockFd 连接套接字
 * @return {*}
 */
int inputNamePwd(msg_t *msg, int sockFd)
{
    char name[20];
    printf("请输入姓名：");
    scanf("%s", name);
    getchar();
    strcpy(msg->staff.name, name);
    char passwd[20];
    printf("请输入密码：");
    scanf("%s", passwd);
    getchar();
    strcpy(msg->staff.password, passwd);
    // 发送请求
    if (write(sockFd, msg, sizeof(msg_t)) < 0)
    {
        return -1;
    }
    // 接收响应
    if (read(sockFd, msg, sizeof(msg_t)) < 0)
    {
        return -1;
    }
    return 0;
}

/**
 * @description: 输入要修改的信息
 * @param {msg_t} *msg
 * @return {*}
 */
void inputUserInfo(msg_t *msg)
{
    printf("请输入需要添加用户的姓名>>\n");
    getchar();
    myFgets(msg->staff.name, sizeof(msg->staff.name), stdin);
    printf("请输入需要添加用户的性别>>\n");
    myFgets(msg->staff.sex, sizeof(msg->staff.sex), stdin);
    printf("请输入需要添加用户的年龄>>\n");
    scanf("%d", &msg->staff.age);
    getchar();
    printf("请输入需要添加用户的部门>>\n");
    myFgets(msg->staff.department, sizeof(msg->staff.department), stdin);

    printf("请输入需要添加用户的密码>>\n");
    myFgets(msg->staff.password, sizeof(msg->staff.password), stdin);

    printf("请输入需要添加用户的薪水>>\n");
    scanf("%d", &(msg->staff.salary));
}

/**
 * @description: 删除用户
 * @param {msg_t} *msg
 * @return {*}
 */
int deleteUser(msg_t *msg)
{
    int mod;

    printf("请输入删除方式:\n");
    printf("请选择根据工号删除(0)或者根据姓名删除(1)>>");
    scanf("%d", &mod);
    switch (mod)
    {
    case 0:
        printf("请输入需要删除用户的工号>>");
        getchar();
        scanf("%d", &(msg->staff.id));
        break;
    case 1:
        printf("请输入需要删除用户的姓名>>");
        getchar();
        myFgets(msg->staff.name, sizeof(msg->staff.name), stdin);
        break;
    default:
        printf("输入错误，请重新输入>>");
        return 0;
    }
    return 0;
}

/**
 * @description: 修改用户信息
 * @param {msg_t} *msg
 * @param {int} fd
 * @return {*}
 */
int modifyUserInfo(msg_t *msg, int fd)
{
    int mod;
    msg->cmd |= SELECT_ONE;
    printf("请输入你要修改的用户的工号：\n");
    scanf("%d", &msg->condition);
    write(fd, msg, sizeof(msg_t));
    read(fd, msg, sizeof(msg_t));
    msg->cmd = CHANGE_USER_INFO;
    ministerModifyChooseShow();
    printf("请输入你要修改的选项>>");
    scanf("%d", &mod);
    switch (mod)
    {
    case 1:
        printf("请输入要修改的用户名>>");
        scanf("%s", msg->staff.name);
        break;
    case 2:
        printf("请输入要修改的年龄>>");
        scanf("%d", &msg->staff.age);
        break;
    case 3:
        printf("请输入要修改的性别>>");
        scanf("%s", msg->staff.sex);
        break;
    case 4:
        printf("请输入要修改的密码>>");
        scanf("%s", msg->staff.password);
        break;
    case 5:
        printf("请输入要修改的工号>>");
        scanf("%d", &msg->staff.id);
        break;
    case 6:
        printf("请输入要修改的部门>>");
        scanf("%s", msg->staff.department);
        break;
    case 7:
        printf("请输入要修改的薪水>>");
        scanf("%d", &msg->staff.salary);
        break;
    case 8:
        return 0;
    default:
        printf("输入错误，请重新输入！");
        break;
    }
    return 0;
}

/**
 * @description: 查询用户信息
 * @param {msg_t} *msg
 * @return {*}
 */
int selectUserInfo(msg_t *msg)
{
    int mod;

    selectChooseShow();
    printf("请输入你要查询的选项>>");
    scanf("%d", &mod);
    switch (mod)
    {
    case 1:
        msg->cmd = (SELECT_USER_INFO);
        break;
    case 2:
        msg->cmd = (SELECT_USER_INFO | TOP_RECORD);
        break;
    case 3:
        msg->cmd = (SELECT_USER_INFO | BOTTOM_RECORD);
        break;
    case 4:
        printf("请输入要查询记录的个数(1-100)>> ");
        scanf("%d", &msg->condition);
        msg->cmd = (SELECT_USER_INFO | BY_CONDITION);
        break;
    }
}

int userHistory(msg_t *msg)
{
    int mod;

    // 1.所有历史记录
    //  2.后50条历史信息
    //  3.按输入条件查询（1-100）
    printf("*************************************************\n");
    printf("*************** 1.所有历史记录 *******************\n");
    printf("*************** 2.后50条历史信息 ******************\n");
    printf("*************** 3.条件查询 ***********************\n");
    printf("*************************************************\n");
    printf("请输入你要查询的选项>>");
    scanf("%d", &mod);
    switch (mod)
    {
    case 1:
        msg->cmd = (SELECT_ALL);
        break;
    case 2:
        msg->cmd = (SELECT_ALL | BOTTOM_RECORD);
        break;
    case 3:
        printf("请输入要查询历史记录的个数(1-100)>> ");
        scanf("%d", &msg->condition);
        msg->cmd = (SELECT_ALL | BY_CONDITION);
        break;
    default:
        printf("输入有误，请重试\n");
        break;
    }
    return 0;
}
/**
 * @description: 管理员视图
 * @param {int} sockFd
 * @param {msg_t} *msg
 * @return {*}
 */
int adminView(int sockFd, msg_t *msg)
{
    int func;
    // 定义数据缓存区1
    char buffer[128] = {0};
    // 定义数据缓存区2
    char buffer2[128] = {0};
    while (1)
    {
        // 打印管理员功能菜单
        initMsg(msg);
        managerShow();

        // 获取输入的功能
        scanf("%d", &func);
        switch (func)
        {
        case 1:
            // 添加用户
            msg->cmd = ADD_USER;
            inputUserInfo(msg);
            write(sockFd, msg, sizeof(msg_t));
            read(sockFd, msg, sizeof(msg_t));
            if (msg->condition)
            {
                printf("添加成功!^^");
            }
            else
            {
                printf("添加失败了呦,请重试..\n");
            }

            break;
        case 2:

            // 删除用户
            initMsg(msg);
            msg->cmd = DELETE_USER;
            deleteUser(msg);

            write(sockFd, msg, sizeof(msg_t));
            read(sockFd, msg, sizeof(msg_t));
            if (msg->condition)
            {
                printf("删除成功!");
            }
            else
            {
                printf("删除失败了呦,请检查用户名是否输入正确..\n");
            }
            break;
        case 3:
            // 修改用户信息     杨泽鹏我把要修改的原先的id写在了condition里
            initMsg(msg);
            msg->cmd = CHANGE_USER_INFO;
            modifyUserInfo(msg, sockFd);
            write(sockFd, msg, sizeof(msg_t));
            read(sockFd, msg, sizeof(msg_t));
            if (msg->condition)
            {
                printf("修改成功！^^\n");
            }
            else
            {
                printf("修改失败！\n");
            }
            break;
        case 4:
            // 查询用户信息
            selectUserInfo(msg);
            write(sockFd, msg, sizeof(msg_t));
            read(sockFd, msg, sizeof(msg_t));
            printf("%s", msg->arr);
            break;
        case 5:
            // 查询历史
            
            userHistory(msg);
            write(sockFd, msg, sizeof(msg_t));
            read(sockFd, msg, sizeof(msg_t));
            printf("%s", msg->arr);
            break;
        case 6:
            // 退出
            return 0;
        default:
            printf("输入错误，请重新输入..\n");
            break;
        }
    }
}

/**
 * @description: 选择注册模式
 * @param {int} sockFd
 * @param {msg_t} *msg
 * @return {*}
 */
int registMod(int sockFd, msg_t *msg)
{
    int mod;
    // 定义一个管理员输入的验证密码变量
    char passwd[18];
    printf("选择注册普通用户(0)还是管理员(1)>\n");
    scanf("%d", &mod);
    // 判断是不是管理员模式
    if (mod)
    {
        // 选择管理员模式后进入密码验证
        printf("请输入管理员密码>\n");
        scanf("%s", passwd);
        getchar();
        if (strcmp(passwd, ADMIN_PASSWORD))
        {
            printf("密码输入错误，请重试..\n");
            return -1;
        }
        msg->cmd = REGISTER | ISADMIN;
    }
    else
    {
        // 普通用户注册
        msg->cmd = REGISTER;
    }
    inputNamePwd(msg, sockFd);
    return 0;
}

/**
 * @description: 普通用户视图
 * @param {int} sockFd
 * @param {msg_t} *msg
 * @return {*}
 */
int staffView(int sockFd, msg_t *msg)
{
    // 定义一个字符用来保存用户选择的功能
    int func, ret;
    // 定义数据缓存区1
    char buffer[128] = {0};
    // 定义数据缓存区2
    char buffer2[128] = {0};
    while (1)
    {
        // 打印用户选择功能菜单
        normalShow();
        // 获取用户输入的功能
        scanf("%d", &func);
        switch (func)
        {
        case 1:
            // 查询信息
            printf("姓名: %s\n", msg->staff.name);
            printf("年龄: %d\n", msg->staff.age);
            printf("性别: %s\n", msg->staff.sex);
            printf("密码: %s\n", msg->staff.password);
            printf("工号: %d\n", msg->staff.id);
            printf("工资: %d\n", msg->staff.salary);
            printf("部门: %s\n", msg->staff.department);
            break;
        case 2:
        tip:
            // 修改密码
            printf("请输入你要改的密码>>");
            scanf("%s", buffer);
            printf("请再输入一次新的密码>>");
            scanf("%s", buffer2);
            // 比对密码
            if (strcmp(buffer, buffer2))
            {
                printf("两次密码不一致,请重新输入\n");
                goto tip;
            }
            strcpy(msg->staff.password, buffer);
            msg->cmd = CHANGE_PASSWORD;
            write(sockFd, msg, sizeof(msg_t));
            read(sockFd, msg, sizeof(msg_t));
            break;
        case 3:
            msg->cmd = CHANGE_INFO;
            // 修改个人信息
            printModifyInfo();
            printf("请输入你要修改的选项>>\n");
            scanf("%d", &ret);
            if (ret == 1)
            {
                memset(msg->staff.name, 0, sizeof(msg->staff.name));
                printf("请输入你要修改的姓名>>\n");
                scanf("%s", msg->staff.name);
            }
            else if (ret == 2)
            {
                printf("请输入你要修改的年龄>>\n");
                scanf("%d", &msg->staff.age);
            }
            else if (ret == 3)
            {
                printf("请输入你要修改的性别>>\n");
                scanf("%s", msg->staff.sex);
            }
            write(sockFd, msg, sizeof(msg_t));
            read(sockFd, msg, sizeof(msg_t));
            if ((msg->condition))
            {
                printf("修改成功^^..您的信息为：工号：%d,姓名：%s,性别：%s,年龄：%d,部门：%s,薪水：%d\n",
                       msg->staff.id, msg->staff.name, msg->staff.sex, msg->staff.age, msg->staff.department, msg->staff.salary);
            }
            else
            {
                printf("修改失败了呦,请重试..\n");
                goto tip;
            }
            break;
        // case 4:
        //    break;
        case 5:
            // 退出
            return 0;
            break;
        default:
            printf("输入错误了呀,请重新输入[1-5] >\n");
            break;
        }
    }
}

/**
 * @description: 登录
 * @param {int} sockFd
 * @param {msg_t} *msg
 * @return {*}
 */
int staffLogin(int sockFd, msg_t *msg)
{

    msg->cmd = LOGIN;
    inputNamePwd(msg, sockFd);

    if ((msg->condition) == 1)
    {
        printf("登录成功^^\n");
    }
    else
    {
        printf("登录失败,请重试..\n");
        return -1;
    }
    // 得到服务器传来的msg的isminister模式
    switch (msg->staff.isadmin)
    {
    case 0:
        printf("欢迎员工登录^^ >\n");
        staffView(sockFd, msg);
        break;

    case 1:
        printf("管理员进入成功^^");
        adminView(sockFd, msg);
    }
}

int main(int argc, char const *argv[])
{
    int sockFd = connectSocket("192.168.250.100", PORT);
    int mod;
    msg_t msg;
    initMsg(&msg);

    printf("欢迎进入员工管理系统^^\n");
    while (1)
    {
        // 打印注册登录退出的表头
        printShow();
        // 获取用户输入的选择
        scanf("%d", &mod);
        switch (mod)
        {
        case 1:
            // 注册
            registMod(sockFd, &msg);
            if (msg.condition)
            {
                printf("注册成功！您的工号为:%d\n", msg.staff.id);
            }
            else
            {
                printf("注册失败啦，试试换个用户名呢^^\n");
                continue;
            }
            break;
        case 2:
            // 登录  选择是管理员还是普通用户，员工具体看到的功能在这个函数里
            staffLogin(sockFd, &msg);
            break;
        case 3:
            // 退出
            close(sockFd);
            printf("退出成功^^\n");
            return 0;
            break;
        default:
            printf("输入错误，请重新输入..\n");
            break;
        }
    }

    close(sockFd);

    return 0;
}
