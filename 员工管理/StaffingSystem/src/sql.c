#include "../inc/sql.h"
int callbackByName(void *data, int argc, char **argv, char **azColName)
{

    msg_t *msg = data;

    strcpy(msg->staff.name, argv[0]);
    msg->staff.age = atoi(argv[1]);
    strcpy(msg->staff.sex, argv[2]);
    strcpy(msg->staff.password, argv[3]);
    msg->staff.id = atoi(argv[4]);
    msg->staff.salary = atoi(argv[5]);
    strcpy(msg->staff.department, argv[6]);
    msg->staff.isadmin = atoi(argv[7]);
    return 0;
}

int queryByNameOrId(msg_t *msg, sqlite3 *db)
{
    char sql[1024];
    sprintf(sql, "select * from staff where name='%s' or id=%d", msg->staff.name, msg->staff.id);
    sqlite3_exec(db, sql, callbackByName, msg, NULL);
    return 0;
}

int insertUser(msg_t *msg, sqlite3 *db)
{
    char *errmsg = NULL;
    char sqlstr[1024];
    sprintf(sqlstr, "INSERT INTO staff(name,age,sex,pwd,salary,department,isadmin) VALUES('%s',%d,'%s','%s',%d,'%s',%d)",
            msg->staff.name, msg->staff.age, msg->staff.sex, msg->staff.password, msg->staff.salary, msg->staff.department, msg->staff.isadmin);
    if (SQLITE_OK != (sqlite3_exec(db, sqlstr, NULL, NULL, &errmsg)))
    {
        perror("sqlite3_exec error\n");
        return -1;
    }
    return 0;
}

int updataUserInfo(msg_t *msg, sqlite3 *db)
{
    char sql[1024];
    sprintf(sql, "update staff set name = '%s', age = %d, sex = '%s', pwd ='%s',\
             id = %d,salary = %d,department = '%s' where id = %d;",
            msg->staff.name, msg->staff.age, msg->staff.sex, msg->staff.password,
            msg->staff.id, msg->staff.salary, msg->staff.department, msg->staff.id);
    int ret = sqlite3_exec(db, sql, NULL, NULL, NULL);
    if (ret != SQLITE_OK)
        return -1;
    return 0;
}
int deleteUser(msg_t *msg, sqlite3 *db)
{
    char sqlstr[256] = {0};

    sprintf(sqlstr, "DELETE FROM staff WHERE name='%s' OR id = %d", msg->staff.name, msg->staff.id); // 实现用户名或者工号删除
    if (SQLITE_OK != (sqlite3_exec(db, sqlstr, NULL, NULL, NULL)))
    {
        perror("sqlite3_exec error\n");
        return -1;
    }

    return 0;
}

int updateByCondition(msg_t *msg, sqlite3 *db)
{
    char sql[1024];
    sprintf(sql, "update staff set name = '%s', age = %d, sex = '%s', pwd ='%s',\
             id = %d,salary = %d,department = '%s' where id = %d;",
            msg->staff.name, msg->staff.age, msg->staff.sex, msg->staff.password,
            msg->staff.id, msg->staff.salary, msg->staff.department, msg->condition);
    int ret = sqlite3_exec(db, sql, NULL, NULL, NULL);
    if (ret != SQLITE_OK)
        return -1;
    return 0;
}

int query_callback(void *data, int argc, char **argv, char **azColName)
{
    msg_t *msg = data;

    char arr[1024];
    sprintf(arr, "%s | %s  %s | %s | %s | %s | %s\n", argv[0], argv[1],
            argv[2], argv[3], argv[4], argv[5], argv[6]);
    strcat(msg->arr, arr);
    return 0;
}
int queryByTopRecord(msg_t *msg, sqlite3 *db)
{
    char sql[1024];
    memset(msg->arr, 0, sizeof(msg->arr));
    sprintf(sql, "select * from staff order by id asc limit 10;");
    int ret = sqlite3_exec(db, sql, query_callback, (void *)msg, NULL);
    if (ret != SQLITE_OK)
        return -1;
    return 0;
}

int queryByCondition(msg_t *msg, sqlite3 *db)
{
    char sql[1024];
    memset(msg->arr, 0, sizeof(msg->arr));
    sprintf(sql, "select * from staff order by id asc limit %d;", msg->condition);
    int ret = sqlite3_exec(db, sql, query_callback, (void *)msg, NULL);
    if (ret != SQLITE_OK)
        return -1;
    return 0;
}

int queryByBottomRecord(msg_t *msg, sqlite3 *db)
{
    char sql[1024];
    memset(msg->arr, 0, sizeof(msg->arr));
    sprintf(sql, "select * from staff order by id desc limit 10;");
    int ret = sqlite3_exec(db, sql, query_callback, (void *)msg, NULL);
    if (ret != SQLITE_OK)
        return -1;
    return 0;
}
int queryAll(msg_t *msg, sqlite3 *db)
{
    char sql[1024];
    memset(msg->arr, 0, sizeof(msg->arr));
    sprintf(sql, "select * from staff order by id asc;");
    int ret = sqlite3_exec(db, sql, query_callback, (void *)msg, NULL);
    if (ret != SQLITE_OK)
        return -1;
    return 0;
}

int query_callbackA(void *data, int argc, char **argv, char **azColName)
{
    msg_t *msg = data;

    char arr[1024];
    sprintf(arr, "%s | %s | %s | %s\n", argv[0], argv[1],
            argv[2], argv[3]);
    strcat(msg->arr, arr);
    return 0;
}

int queryHistoryCondition(msg_t *msg, sqlite3 *db)
{
    char sql[1024];
    memset(msg->arr, 0, sizeof(msg->arr));
    sprintf(sql, "select * from staff_log limit %d;", msg->condition);
    int ret = sqlite3_exec(db, sql, query_callbackA, (void *)msg, NULL);
    if (ret != SQLITE_OK)
        return -1;
    return 0;
}

int queryHistoryBottom(msg_t *msg, sqlite3 *db)
{
    char sql[1024];
    memset(msg->arr, 0, sizeof(msg->arr));
    sprintf(sql, "select * from staff_log limit %d;", msg->condition);
    int ret = sqlite3_exec(db, sql, query_callbackA, (void *)msg, NULL);
    if (ret != SQLITE_OK)
        return -1;
    return 0;
}

int queryHistoryAll(msg_t *msg, sqlite3 *db)
{
    char sql[1024];
    memset(msg->arr, 0, sizeof(msg->arr));
    sprintf(sql, "select * from staff_log ;");
    int ret = sqlite3_exec(db, sql, query_callbackA, (void *)msg, NULL);
    if (ret != SQLITE_OK)
        return -1;
    return 0;
}