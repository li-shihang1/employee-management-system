#include "../inc/head.h"
#include "../inc/handler.h"

#define DATABASE_PATH "../staffing-system.db"
/**
 * @description: 初始化epoll服务器
 * @param {int} sockfd
 * @return {int} epollfd
 */
int initEpoll(int sockfd)
{
    struct sockaddr_in serverInfo = {0};
    serverInfo.sin_family = AF_INET;
    serverInfo.sin_port = htons(PORT);
    serverInfo.sin_addr.s_addr = inet_addr("192.168.250.100");
    int ret = bind(sockfd, (const struct sockaddr *)&serverInfo, sizeof(serverInfo));
    if (ret == -1)
    {
        PRINT_ERR("bind err..\n");
        return -1;
    }
    if ((listen(sockfd, 5)) == -1)
    {
        PRINT_ERR("listen error..\n");
        return -1;
    }
    // 创建文件描述符集合:epoll实例（）
    int epollfd = epoll_create1(0);
    if (-1 == epollfd)
    {
        perror("epoll_create1 error");
        return -1;
    }

    return epollfd;
}

/**
 * @description: 打开数据库
 * @return {sqlite3 *} db
 */
sqlite3 *initDatabase()
{
    sqlite3 *db;
    int ret = sqlite3_open(DATABASE_PATH, &db);
    if (ret != SQLITE_OK)
        return NULL;

    return db;
}

int main()
{
    int id = 0;//保存修改前的查到的Id
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        PRINT_ERR("socket error..\n");
        return -1;
    }
    int epollfd = initEpoll(sockfd);
    // 把关心的文件描述符放到epoll实例
    struct epoll_event ev = {0};
    ev.events = EPOLLIN;
    ev.data.fd = sockfd;
    if ((epoll_ctl(epollfd, EPOLL_CTL_ADD, sockfd, &ev)) == -1)
    {
        perror("EPOLL_CTL_ADD error");
        return -1;
    }
    // 定义一个事件描述符集合数组
    struct epoll_event eventFdArray[EVENTDE_LEN] = {0};
    msg_t msg = {0};
    // 定义一个数据库连接
    sqlite3 *db = initDatabase();
    
    
    // 进入epoll服务器循环
    printf("epoll启动\n");
    while (1)
    {
        int nfds = epoll_wait(epollfd, eventFdArray, EVENTDE_LEN, -1);
        if (nfds == -1)
        {
            perror("epoll_wait error");
            return -1;
        }
        for (int i = 0; i < nfds; i++)
        {
            if (eventFdArray[i].data.fd == sockfd)
            {
                printf("epoll服务器收到客户端连接请求\n");
                int connect_fd = accept(sockfd, NULL, NULL);
                if (connect_fd == -1)
                {
                    perror("accept error");
                    continue;
                }
                ev.data.fd = connect_fd;
                ev.events = EPOLLIN;
                if ((epoll_ctl(epollfd, EPOLL_CTL_ADD, connect_fd, &ev)) == -1)
                {
                    perror("EPOLL_CTL_ADD error");
                    continue;
                }
            }
            else
            {
                memset(&msg, 0, sizeof(msg));

                int ret = read(eventFdArray[i].data.fd, &msg, sizeof(msg));
                if (ret == -1)
                {
                    perror("error");
                    continue;
                }
                if (ret == 0)
                {
                    // 客户端关闭
                    if ((epoll_ctl(epollfd, EPOLL_CTL_DEL, eventFdArray[i].data.fd, &eventFdArray[i])) == -1)
                    {
                        perror("error");
                        continue;
                    }
                    close(eventFdArray[i].data.fd);
                    continue;
                }
                printf("客户端[%d]发来消息\n", eventFdArray[i].data.fd);
                switch (msg.cmd & ~(0xffff << 16))
                {
                case REGISTER:
                    userRegister(&msg, eventFdArray[i].data.fd, db);
                    write(eventFdArray[i].data.fd, &msg, sizeof(msg_t));
                    break;
                case LOGIN:
                    printf("登录\n");
                    userLogin(&msg, eventFdArray[i].data.fd, db);
                    write(eventFdArray[i].data.fd, &msg, sizeof(msg_t));
                    break;
                case CHANGE_PASSWORD:

                    printf("修改密码\n");
                    changePassword(&msg, eventFdArray[i].data.fd, db);
                    write(eventFdArray[i].data.fd, &msg, sizeof(msg_t));
                    break;
                case CHANGE_INFO:
                    printf("修改个人信息\n");
                    changeInfo(&msg, eventFdArray[i].data.fd, db);
                    write(eventFdArray[i].data.fd, &msg, sizeof(msg_t));

                    break;
                case SELECT_HISTORY:
                    printf("查询历史\n");
                    // searchHistory(&msg, eventFdArray[i].data.fd, db);
                    break;
                case ADD_USER:
                    addUser(&msg, eventFdArray[i].data.fd, db);
                    write(eventFdArray[i].data.fd, &msg, sizeof(msg_t));
                    printf("添加用户\n");
                    break;
                case DELETE_USER:
                    deletUser(&msg, eventFdArray[i].data.fd, db);
                    write(eventFdArray[i].data.fd, &msg, sizeof(msg_t));
                    printf("删除用户\n");
                    break;
                case CHANGE_USER_INFO:
                    printf("修改用户信息\n");
                    
                    modifyInfo(&msg, eventFdArray[i].data.fd, db);
                    write(eventFdArray[i].data.fd, &msg, sizeof(msg_t));
                    break;
                case SELECT_USER_INFO:
                    printf("查询用户信息\n");
                    selectUserInfo(&msg, eventFdArray[i].data.fd, db);
                    write(eventFdArray[i].data.fd, &msg, sizeof(msg_t));

                    break;
                case SELECT_ALL:
                    searchHistory(&msg, eventFdArray[i].data.fd, db);
                    write(eventFdArray[i].data.fd, &msg, sizeof(msg_t));
                    printf("查询所有用户\n");
                    break;
                }
            }
        }
    }
    close(sockfd);

    return 0;
}