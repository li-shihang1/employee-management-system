#ifndef __HEAD_H__
#define __HEAD_H__

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <pthread.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sqlite3.h>
#include <sys/epoll.h>



#define PORT 8686

// admin密码
#define ADMIN_PASSWORD "123456"

// EPOLL最大连接数
#define EVENTDE_LEN 1024

// 命令码高16位
#define ISADMIN 0X10000            // 是否是管理员用户 
#define TOP_RECORD ISADMIN         // 从表前开始查找
#define BOTTOM_RECORD 0X20000      // 从表尾开始查找
#define BY_CONDITION 0X30000       // 安条件查找
#define SELECT_ONE 0X40000         // 查询一条记录


// 员工结构体(包括管理员)
typedef struct
{
    char name[64];
    int age;
    char sex[10];
    char password[42];
    int id;
    int salary;
    char department[128];
    int isadmin;
} staff_t;

// 命令码低16位
typedef enum
{
    REGISTER = 1,       // 注册
    LOGIN,              // 登录
    CHANGE_PASSWORD,    // 修改密码
    CHANGE_INFO,        // 修改个人信息
    SELECT_HISTORY,     // 查询历史记录
    ADD_USER,           // 添加员工
    DELETE_USER,        // 删除员工
    CHANGE_USER_INFO,   // 修改员工信息
    SELECT_USER_INFO,   // 查询员工信息
    SELECT_ALL          // 所有历史记录
} cmd_t;

// 消息结构体
typedef struct
{
    unsigned int cmd;    // 命令码
    void *condition;     // 条件
    staff_t staff;      // 个人信息
    char arr[1024];
} msg_t;

#define PRINT_ERR(errmsg)                                   \
    do                                                      \
    {                                                       \
        printf("%s:%s:%d\n", __FILE__, __func__, __LINE__); \
        perror(errmsg);                                     \
        return -1;                                          \
    } while (0)

#endif
