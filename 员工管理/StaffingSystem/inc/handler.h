#ifndef __HANDLER_H__
#define __HANDLER_H__
#include "head.h"
int userRegister(msg_t *msg, int fd, sqlite3 *db);
int userLogin(msg_t *msg, int fd,sqlite3 *db);

int changePassword(msg_t *msg,int fd, sqlite3 *db);
int changeInfo(msg_t *msg,int fd, sqlite3 *db);
int searchHistory(msg_t *msg,int fd, sqlite3 *db);
int addUser(msg_t *msg, int fd, sqlite3 *db);
int deletUser(msg_t *msg, int fd, sqlite3 *db);
int modifyInfo(msg_t *msg, int fd, sqlite3 *db);
int selectUserInfo(msg_t *msg, int fd, sqlite3 *db);
int initMsg(msg_t *msg);

#endif