#ifndef __SQL_H__
#define __SQL_H__
#include "head.h"

int callbackByName(void *data, int argc, char **argv, char **azColName);
int queryByNameOrId(msg_t *msg, sqlite3 *db);
int insertUser(msg_t *msg, sqlite3 *db);
int updataUserInfo(msg_t *msg, sqlite3 *db);
int deleteUser(msg_t *msg, sqlite3 *db);
int updateByCondition(msg_t *msg, sqlite3 *db);
int queryByTopRecord(msg_t *msg, sqlite3 *db);
int query_callback(void *data, int argc, char **argv, char **azColName);
int queryByCondition(msg_t *msg, sqlite3 *db);
int queryByBottomRecord(msg_t *msg, sqlite3 *db);
int queryAll(msg_t *msg, sqlite3 *db);

int queryHistoryBottom(msg_t *msg, sqlite3 *db);
int queryHistoryAll(msg_t *msg, sqlite3 *db);
int queryHistoryCondition(msg_t *msg, sqlite3 *db);
#endif